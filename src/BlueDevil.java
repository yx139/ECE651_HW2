import java.util.Scanner;

public class BlueDevil extends Person{

	protected String major;
	protected String degree;
	
	protected boolean hasWorked = false;
	protected String workplace;
	
	// constructor.
	public BlueDevil(String firstname, String lastname) {
		this.firstName = firstname;
		this.lastName = lastname;
	}
	
	// parse input.
	public void addDevil() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter User's Age: ");
		this.age = Integer.getInteger(scanner.nextLine());
		
		System.out.println("Enter User's Nationality: ");
		this.nationality = scanner.nextLine();
		
		System.out.println("Enter User's Undergrad Experience: ");
		this.undergrad = scanner.nextLine();
		
		System.out.println("Enter User's Hobby: ");
		this.hobby = scanner.nextLine();

		System.out.println("Enter User's Major in Duke: ");
		this.major = scanner.nextLine();
		
		System.out.println("Enter User's Degree in Duke: ");
		this.degree = scanner.nextLine();
		
		System.out.println("Enter User's Work Experience: (if no expeirence, simply hit ENTER)");
		String workexp = scanner.nextLine();
		this.hasWorked = workexp.equals("") ? false : true;
		this.workplace = workexp;
	}
	
	// current education.	
	public void getEducation() {
		if(this.major == null || this.degree == null) {
			System.out.println("No basic information added yet, please add relative info.");
		}
		
		this.getPerson();
		System.out.print(", and is a " + this.degree + " in " + this.major + ".\n");
	}
	
	// work experience.
	public void getWork() {
		if(!hasWorked) {
			System.out.println(this.firstName + " does not have prior work experience.");
		}
		else {
			System.out.println(this.firstName + " have prior work experience in " + this.workplace + ".");
		}
	}
	
	// previous education.
	public void getPreEducation() {
		if(this.undergrad == null)
			return;
		
		System.out.println(getName() + " recieved undergraduate from " + this.undergrad + ".");
	}
	
	// hobby.
	public void getHobby() {
		if(this.hobby == null)
			return;
		
		System.out.println("When not in class, " + this.firstName + " enjoys " + this.hobby + ".");
	}
}
