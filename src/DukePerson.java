import java.util.Scanner;
import java.util.HashMap; 


public class DukePerson {
	
	protected static HashMap<String, BlueDevil> map = new HashMap<>();
	
	protected static void whoIs(String name) {
		if(map.containsKey(name)) {
			BlueDevil devil = map.get(name);
			
			devil.getEducation();
			devil.getWork();
			devil.getPreEducation();
			devil.getHobby();
		}
		else {
			System.out.println("No matching person found.");
		}
	}

	public static void addPerson(String[] attrs) {
		
		BlueDevil person = new BlueDevil(attrs[1], attrs[2]);
		person.addDevil();
		
		map.put(person.getName(), person);
		System.out.println(person.getName() + " added.");
	}
	
	public static void deletePerson(String name) {
		if(map.containsKey(name)) {
			map.remove(name);
			System.out.println(name + " deleted.");
		}
		else {
			System.out.println("No matching person found.");
		}
	}
	
	
	public static void parse(String command) {
		String[] attrs = command.split(" ");
		if(attrs.length != 3) {
			System.out.println("Wrong Format! Please re-enter.");
			return;
		}
		
		String comm = attrs[0];
		String name = attrs[1] + " " + attrs[2];

		if(comm.equals("add")) {
			addPerson(attrs);
		}
		if(comm.equals("delete")) {
			deletePerson(name);
		}
		if(comm.equals("whois")) {
			whoIs(name);
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub				

		try ( Scanner scanner = new Scanner(System.in); ) {
			while(true) {
				System.out.println("\nEnter Command: \n[COMMAND](add/delete/whois) [FIRST NAME] [LASTNAME]");
				String command = scanner.nextLine();
				parse(command);
			}
		}
		catch(Exception ex) {
		    //exception handling...do something (e.g., print the error message)
		    System.out.println("Error occured in scanner.");
		}
	}

}
