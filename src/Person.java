
public class Person {
	
	protected String firstName;
	protected String lastName;
	
	protected Integer age;
	
	protected String nationality;
	
	protected String undergrad;
	
	protected String hobby;
	
	
	public String getName() {
		return this.firstName + " " + this.lastName;
	}
	
	public String getNation() {
		return this.nationality;
	}
	
	public void getPerson() {
		if(this.nationality == null) {
			System.out.println("No basic information added yet, please add relative info.");
		}
		System.out.print("\n" + this.getName() + " is from " + this.getNation());
	}
}
