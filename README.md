### ECE651_HW2

#### COMPLING
_________________

To compile this, run:
`sudo javac -d bin -sourcepath src src/DukePerson.java src/Person.java src/BlueDevil.java`

##### Running
________________
To run this code, use:

###### Linux / OSX

`java -cp bin DukePerson`


#### TESTING
________________

Mainly three operation can be completed.

    - add
    - whois
    - delete

1. input `add yifan xiao`.

    The output instruction will guide you to fill out relevant information.

2. input `whois yifan xiao`.

    Program will try to search if `yifan xiao` exists in the Data Structure.

    If exists, the output will display all information about that person.

    If not, the output will show `no person found.`

3. input `delete yifan xiao`.

    The output will show `yifan xiao is deleted.`
    And when trying to type `whois yifan xiao` will output 'no person found'.